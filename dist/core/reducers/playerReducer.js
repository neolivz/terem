"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var actionTypes_1 = require("../actions/actionTypes");
exports.playerReducer = function (state, action) {
    switch (action.type) {
        case actionTypes_1.SAVE_PLAYER: {
            var player = action.payload;
            if (action.status === actionTypes_1.ASYNC_STARTED) {
                return undefined; //Successful remove the player from the store
            }
            else if (action.status === actionTypes_1.ASYNC_FAILED) {
                return player; //We change to edit to back the same player
            }
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }
        }
        default:
            return state;
    }
};
exports.default = exports.playerReducer;
//# sourceMappingURL=playerReducer.js.map
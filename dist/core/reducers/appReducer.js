"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var types = require("../actions/actionTypes");
var initialState = {
    status: 'Initial',
    lastUpdatedOn: new Date()
};
exports.appReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case types.APP_START_KICKED:
            return __assign({}, state, { status: 'Starting', lastUpdatedOn: new Date() });
        case types.APP_STARTED:
            return __assign({}, state, { status: 'Started', lastUpdatedOn: new Date() });
        case types.APP_START_FAILED:
            return __assign({}, state, { status: 'FailedToStart', lastUpdatedOn: new Date(), errorReason: action.reason });
        default:
            return state;
    }
};
//# sourceMappingURL=appReducer.js.map
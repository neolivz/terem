"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_router_redux_1 = require("react-router-redux");
var redux_1 = require("redux");
var redux_form_1 = require("redux-form");
var appReducer_1 = require("./appReducer");
var gameReducer_1 = require("./gameReducer");
var gamesReducer_1 = require("./gamesReducer");
var playerReducer_1 = require("./playerReducer");
var playersReducer_1 = require("./playersReducer");
exports.rootReducer = redux_1.combineReducers({
    app: appReducer_1.appReducer,
    form: redux_form_1.reducer,
    routing: react_router_redux_1.routerReducer,
    game: gameReducer_1.gameReducer,
    games: gamesReducer_1.gamesReducer,
    player: playerReducer_1.playerReducer,
    players: playersReducer_1.playersReducer,
});
//# sourceMappingURL=index.js.map
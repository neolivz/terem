"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var actionTypes_1 = require("../actions/actionTypes");
exports.playersReducer = function (state, action) {
    switch (action.type) {
        case actionTypes_1.SAVE_PLAYER: {
            var player = action.payload;
            if (action.status === actionTypes_1.ASYNC_STARTED) {
                return state.concat([player]);
            }
            else if (action.status === actionTypes_1.ASYNC_FAILED) {
                var index = state.indexOf(player);
                return state.slice(0, index).concat(state.slice(index + 1));
            }
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }
        }
        case actionTypes_1.LOAD_PLAYERS: {
            var players = action.payload;
            if (action.status === actionTypes_1.ASYNC_STARTED) {
                return []; //We are still loading players
            }
            else if (action.status === actionTypes_1.ASYNC_FAILED) {
                return state; //Return current state of the store
            }
            else {
                return players; //We have the new players loaded
            }
        }
        default:
            return state;
    }
};
exports.default = exports.playersReducer;
//# sourceMappingURL=playersReducer.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var actionTypes_1 = require("../actions/actionTypes");
exports.gamesReducer = function (state, action) {
    switch (action.type) {
        case actionTypes_1.SAVE_GAME: {
            var game = action.payload;
            if (action.status === actionTypes_1.ASYNC_STARTED) {
                return state.concat([game]); //We show the game when async starts
            }
            else if (action.status === actionTypes_1.ASYNC_FAILED) {
                var index = state.indexOf(game);
                return state.slice(0, index).concat(state.slice(index + 1)); //And removed it if it fails
            }
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }
        }
        case actionTypes_1.LOAD_GAMES: {
            var games = action.payload;
            if (action.status === actionTypes_1.ASYNC_STARTED) {
                return []; //We are still loading players
            }
            else if (action.status === actionTypes_1.ASYNC_FAILED) {
                return state; //Return current state of the store
            }
            else {
                return games; //We have the new players loaded
            }
        }
        default:
            return state;
    }
};
exports.default = exports.gamesReducer;
//# sourceMappingURL=gamesReducer.js.map
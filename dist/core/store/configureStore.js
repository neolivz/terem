"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var configureStore_dev_1 = require("./configureStore.dev");
var configureStore_prod_1 = require("./configureStore.prod");
exports.default = (process.env.NODE_ENV === 'production') ? configureStore_prod_1.default : configureStore_dev_1.default;
//# sourceMappingURL=configureStore.js.map
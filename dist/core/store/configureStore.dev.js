"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var reduxImmutableStateInvariant = require("redux-immutable-state-invariant");
var createLogger = require("redux-logger");
var redux_thunk_1 = require("redux-thunk");
var reducers_1 = require("../reducers");
var logger = createLogger();
var devExtension = window.devToolsExtension;
var middlewares = redux_1.applyMiddleware(redux_thunk_1.default, reduxImmutableStateInvariant(), logger);
var configureStore = function (initialState) { return redux_1.createStore(reducers_1.rootReducer, initialState, redux_1.compose(middlewares, devExtension ? devExtension() : function (f) { return f; })); };
exports.default = configureStore;
//# sourceMappingURL=configureStore.dev.js.map
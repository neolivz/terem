"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_thunk_1 = require("redux-thunk");
var reducers_1 = require("../reducers");
var middlewares = redux_1.applyMiddleware(redux_thunk_1.default);
var configureStore = function (initialState) { return redux_1.createStore(reducers_1.rootReducer, initialState, redux_1.compose(middlewares)); };
exports.default = configureStore;
//# sourceMappingURL=configureStore.prod.js.map
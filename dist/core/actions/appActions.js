"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var types = require("./actionTypes");
exports.startApp = function () { return ({
    type: types.APP_START_KICKED
}); };
exports.setAppStarted = function () { return ({
    type: types.APP_STARTED
}); };
exports.failAppStartup = function (reason) { return ({
    type: types.APP_START_FAILED,
    reason: reason
}); };
//# sourceMappingURL=appActions.js.map
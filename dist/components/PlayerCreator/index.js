"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Dialog_1 = require("material-ui/Dialog");
var RaisedButton_1 = require("material-ui/RaisedButton");
var TextField_1 = require("material-ui/TextField");
var style = require("./style");
exports.PlayerCreator = function (props) {
    var actions = [
        React.createElement(RaisedButton_1.default, { label: "Save", primary: true, keyboardFocused: true, onTouchTap: function () { props.onSave({ name: 'Test' }); } }),
        React.createElement(RaisedButton_1.default, { label: "Cancel", secondary: true, keyboardFocused: true, onTouchTap: props.onClose })
    ];
    return (React.createElement("div", { className: style.root },
        React.createElement(Dialog_1.default, { title: "Dialog With Date Picker", actions: actions, modal: false, open: props.open, onRequestClose: props.onClose },
            React.createElement(TextField_1.default, { onChange: function (event, val) { event.preventDefault(); props.onChange(val); }, hintText: "Enter the player name", floatingLabelText: "Player Name", floatingLabelFixed: false }))));
};
exports.default = exports.PlayerCreator;
//# sourceMappingURL=index.js.map
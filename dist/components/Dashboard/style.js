"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typestyle_1 = require("typestyle");
exports.root = typestyle_1.style({
    backgroundColor: '#E1F5FE',
    height: '100vh',
    top: 0,
    left: 0,
    width: '100%'
});
exports.paddingContainer = typestyle_1.style({
    backgroundColor: 'red',
    height: '3em',
    width: '100%'
});
exports.createButton = typestyle_1.style({
    position: 'absolute',
    bottom: 45,
    right: 50
});
//# sourceMappingURL=style.js.map
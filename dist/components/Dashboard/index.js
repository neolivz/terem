"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var react_router_1 = require("react-router");
var FloatingActionButton_1 = require("material-ui/FloatingActionButton");
var FontIcon_1 = require("material-ui/FontIcon");
var style = require("./style");
// interface DashboardProps {
//     readonly to: string;
// }
var Dashboard = (function (_super) {
    __extends(Dashboard, _super);
    function Dashboard() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Dashboard.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("div", null, "Dashboard"),
            React.createElement(react_router_1.Link, { to: "/game-center" },
                React.createElement(FloatingActionButton_1.default, { secondary: true, className: style.createButton },
                    React.createElement(FontIcon_1.default, { className: "fa fa-plus" })))));
    };
    return Dashboard;
}(React.Component));
Dashboard = __decorate([
    react_redux_1.connect()
], Dashboard);
exports.default = Dashboard;
//# sourceMappingURL=index.js.map
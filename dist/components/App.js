"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var AppBar_1 = require("material-ui/AppBar");
var MuiThemeProvider_1 = require("material-ui/styles/MuiThemeProvider");
var colors_1 = require("material-ui/styles/colors");
var getMuiTheme_1 = require("material-ui/styles/getMuiTheme");
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();
var muiTheme = getMuiTheme_1.default({
    palette: {
        primary1Color: colors_1.lightBlueA700,
    },
    appBar: {
        height: 50,
    },
});
var App = (function (_super) {
    __extends(App, _super);
    function App() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    App.prototype.render = function () {
        var children = this.props.children;
        return (React.createElement(MuiThemeProvider_1.default, { muiTheme: muiTheme },
            React.createElement("div", null,
                React.createElement(AppBar_1.default, { title: "Foosball Score", onLeftIconButtonTouchTap: function () { }, iconElementLeft: React.createElement("div", null) }),
                children)));
    };
    return App;
}(React.Component));
exports.default = App;
//# sourceMappingURL=App.js.map
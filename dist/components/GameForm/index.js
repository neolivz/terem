"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var DatePicker_1 = require("material-ui/DatePicker");
var Paper_1 = require("material-ui/Paper");
var RaisedButton_1 = require("material-ui/RaisedButton");
var PlayerSelector_1 = require("../PlayerSelector");
var Score_1 = require("../Score");
var style = require("./style");
var filterPlayers = function (players, filterPlayerArray) {
    return players.filter(function (player) {
        return filterPlayerArray.find(function (filterPlayer) {
            return !filterPlayer || filterPlayer.name === player.name;
        }) == undefined;
    });
};
exports.GameForm = function (props) { return (React.createElement(Paper_1.default, { className: style.root, zDepth: 4 },
    React.createElement("div", { className: style.gameConatiner },
        React.createElement("div", { className: style.playersContainer },
            React.createElement("div", { className: style.player },
                React.createElement(PlayerSelector_1.PlayerSelector, { label: "Player1", players: filterPlayers(props.players, [props.player2, props.player3, props.player4]), activePlayer: props.player1, onChange: function (player) { props.onChange('player1', player); } })),
            React.createElement("div", { className: style.player },
                React.createElement(PlayerSelector_1.PlayerSelector, { label: "Player2 (Optional)", optional: true, players: filterPlayers(props.players, [props.player1, props.player3, props.player4]), activePlayer: props.player2, onChange: function (player) { props.onChange('player2', player); } }))),
        React.createElement("div", { className: style.centerContainer },
            React.createElement("div", { className: style.scoreContainer },
                React.createElement(Score_1.Score, { score: 1 }),
                React.createElement("div", { className: style.vs }, "Vs"),
                React.createElement(Score_1.Score, { score: 0 })),
            React.createElement(DatePicker_1.default, { hintText: "Date", container: "inline", mode: "landscape" })),
        React.createElement("div", { className: style.playersContainer },
            React.createElement("div", { className: style.player },
                React.createElement(PlayerSelector_1.PlayerSelector, { label: "Player3", players: filterPlayers(props.players, [props.player1, props.player2, props.player4]), activePlayer: props.player3, onChange: function (player) { props.onChange('player3', player); } })),
            React.createElement("div", { className: style.player },
                React.createElement(PlayerSelector_1.PlayerSelector, { label: "Player4 (Optional)", optional: true, players: filterPlayers(props.players, [props.player1, props.player2, props.player3]), activePlayer: props.player4, onChange: function (player) { props.onChange('player4', player); } })))),
    React.createElement("div", { className: style.actionsContainer },
        React.createElement(RaisedButton_1.default, { label: "Create", primary: true, style: style })))); };
exports.default = exports.GameForm;
//# sourceMappingURL=index.js.map
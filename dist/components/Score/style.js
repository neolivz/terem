"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typestyle_1 = require("typestyle");
exports.scoreWrapper = typestyle_1.style({
    height: 100,
    width: 100,
    margin: 20,
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
});
exports.score = typestyle_1.style({
    textAlign: 'center',
    fontSize: '1em',
    border: 'none',
    background: 'transparent',
    paddingLeft: '0.6em',
});
//# sourceMappingURL=style.js.map
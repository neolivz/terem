"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Paper_1 = require("material-ui/Paper");
var style = require("./style");
exports.Score = function (props) { return (React.createElement(Paper_1.default, { className: style.scoreWrapper, zDepth: 5, circle: true },
    React.createElement("input", { className: style.score, value: props.score, type: "number" }))); };
exports.default = exports.Score;
//# sourceMappingURL=index.js.map
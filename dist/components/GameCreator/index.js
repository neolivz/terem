"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var GameForm_1 = require("../GameForm");
var GameCreator = (function (_super) {
    __extends(GameCreator, _super);
    function GameCreator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GameCreator.prototype.render = function () {
        var _a = this.props.game, player1 = _a.player1, player2 = _a.player2, player3 = _a.player3, player4 = _a.player4, date = _a.date, score1 = _a.score1, score2 = _a.score2;
        var _b = this.props, players = _b.players, actions = _b.actions;
        var onChange = function () {
            actions.onChange({
                player1: player1, player2: player2, player3: player3, player4: player4, date: date, score1: score1, score2: score2
            });
        };
        return React.createElement("div", null,
            React.createElement(GameForm_1.default, { players: players, player1: player1, player2: player2, player3: player3, player4: player4, score1: score1, score2: score2, date: date, onChange: onChange, onSave: actions.onSave }));
    };
    return GameCreator;
}(React.Component));
exports.default = GameCreator;
//# sourceMappingURL=index.js.map
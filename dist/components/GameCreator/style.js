"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typestyle_1 = require("typestyle");
exports.scoreContainer = typestyle_1.style({
    display: 'flex',
    flexGrow: 0,
    textAlign: 'center',
});
exports.scoreWrapper = typestyle_1.style({
    height: 100,
    width: 100,
    margin: 20,
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
});
exports.score = typestyle_1.style({
    textAlign: 'center',
    fontSize: '1em',
    border: 'none',
    background: 'transparent',
    paddingLeft: '0.6em',
});
exports.vs = typestyle_1.style({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
});
exports.root = typestyle_1.style({
    $debugName: 'root_game_',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#fff',
    margin: '2em',
    paddingLeft: '1em',
    paddingRight: '1em',
    paddingBottom: '2em',
    borderRadius: 100,
});
exports.centerContainer = typestyle_1.style({
    flexGrow: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    display: 'flex',
    paddingLeft: '1em',
    paddingRight: '1em',
    fontSize: '2em',
});
exports.actionsContainer = typestyle_1.style({
    display: 'flex',
    flexDirection: 'row-reverse',
});
exports.gameConatiner = typestyle_1.style({
    display: 'flex',
    width: 'auto',
    $debugName: 'gameConatiner_',
});
exports.playersContainer = typestyle_1.style({
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
});
exports.player = typestyle_1.style({
    display: 'flex',
    flexGrow: 1,
});
exports.playerSelector = typestyle_1.style({
    backgroundColor: '#fff',
    $debugName: 'playerSelector_',
});
//# sourceMappingURL=style.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var MenuItem_1 = require("material-ui/MenuItem");
var SelectField_1 = require("material-ui/SelectField");
exports.PlayerSelector = function (props) {
    return (React.createElement(SelectField_1.default, { labelStyle: { backgroundColor: '#fff', color: '#000' }, style: { color: '#000' }, floatingLabelText: props.label, value: props.activePlayer ? props.activePlayer.name : null, onChange: function (event, key, val) {
            console.log(event, key); //TODO: Not sure how to remove the warning
            props.onChange({ name: val });
        }, fullWidth: true },
        props.optional && React.createElement(MenuItem_1.default, { value: null, primaryText: "" }),
        props.players.map(function (player) { return React.createElement(MenuItem_1.default, { key: player.name, value: player.name, primaryText: player.name }); })));
};
exports.default = exports.PlayerSelector;
//# sourceMappingURL=index.js.map
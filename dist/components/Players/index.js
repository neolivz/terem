"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Hello = function (_a) {
    var to = _a.to;
    return React.createElement("div", null,
        "Hello ",
        to,
        "!");
};
var Players = (function (_super) {
    __extends(Players, _super);
    function Players() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Players.prototype.render = function () {
        return React.createElement("div", null,
            React.createElement("div", null,
                React.createElement(Hello, { to: "World" })),
            React.createElement("div", null, "This is the home page!"));
    };
    return Players;
}(React.Component));
exports.default = Players;
//# sourceMappingURL=index.js.map
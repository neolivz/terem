"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-unused-variable
var React = require("react");
var ReactDom = require("react-dom");
var react_redux_1 = require("react-redux");
var react_router_1 = require("react-router");
var appActions_1 = require("./core/actions/appActions");
var configureStore_1 = require("./core/store/configureStore");
var routes_1 = require("./routes");
var store = configureStore_1.default({});
store.dispatch(appActions_1.startApp());
window.setTimeout(function () { return store.dispatch(appActions_1.setAppStarted()); }, 2000);
ReactDom.render(React.createElement(react_redux_1.Provider, { store: store },
    React.createElement(react_router_1.Router, { routes: routes_1.default, history: react_router_1.browserHistory })), document.getElementById('app'));
//# sourceMappingURL=index.js.map
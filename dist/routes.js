"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-unused-variable
var React = require("react");
var react_router_1 = require("react-router");
var App_1 = require("./components/App");
var Dashboard_1 = require("./components/Dashboard");
var GameCreator_1 = require("./components/GameCreator");
var Players_1 = require("./components/Players");
exports.default = React.createElement(react_router_1.Route, { path: "/", component: App_1.default },
    React.createElement(react_router_1.IndexRedirect, { to: "/home" }),
    React.createElement(react_router_1.Route, { path: "/home", component: Dashboard_1.default }),
    React.createElement(react_router_1.Route, { path: "/game-center", component: GameCreator_1.default }),
    React.createElement(react_router_1.Route, { path: "/players", component: Players_1.default }));
//# sourceMappingURL=routes.js.map
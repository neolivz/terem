// tslint:disable-next-line:no-unused-variable
import * as React from 'react';
import { IndexRedirect, Route } from 'react-router';
import App from './components/App';
import Dashboard from './components/Dashboard';
import GameCreator from './components/GameCreator';
import Players from './components/Players';

export default <Route path="/" component={App}>
    <IndexRedirect to="/home"/>
    <Route path="/home" component={Dashboard}/>
    <Route path="/game-center" component={GameCreator}/>
    <Route path="/players" component={Players}/>
</Route>;

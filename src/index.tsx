// tslint:disable-next-line:no-unused-variable
import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory, Router } from 'react-router';
import { loadPlayers } from './core/actions/playerAction';
import { loadScores } from './core/actions/scoresActions';
import configureStore from './core/store/configureStore';
import routes from './routes';

const store = configureStore({
    app:{ openCreatePlayerDialog:false },
    game:{ score1:0, score2:0, date:new Date() },
    games:[],
    player: { 'name':'' },
    players:[],
    scores:{},
});

store.dispatch(loadPlayers());
store.dispatch(loadScores());

ReactDom.render(
    <Provider store={store}>
        <Router routes={routes} history={browserHistory} />
    </Provider>,
    document.getElementById('app')
);

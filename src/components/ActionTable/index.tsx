import * as React from 'react';


import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


import { VS_WITH } from '../../core/actions/playerAction';
import { Player } from '../../core/reducers/playerReducer';
import { ScoreSheet, ScoreSheets,  } from '../../core/reducers/scoresReducer';

import PlayerSelector from '../PlayerSelector/';

import * as style from './style';

interface ActionTableProperties {
    scores: ScoreSheets;
    player?:Player;
    filter?:VS_WITH;
    onPlayerSelect:(player:Player)=>any;
    onModeSelect:(vswith:VS_WITH) => any;
}

const makeActivePlayers = (scores:ScoreSheets):Player[]=>{
    let players:Player[] = [];
    Object.keys(scores).map((key)=>{
        players.push(scores[key].player)
    })
    return players;
}


export const ActionTable = (props: ActionTableProperties) => {
    const { filter = 'vs', onPlayerSelect, onModeSelect } = props;
    let { scores } = props;
    const players = makeActivePlayers(scores);

    if(props.player){
        scores = scores[props.player.name]?scores[props.player.name][filter]:{};
    }

    return (
        <div>{filter}
        <div className={style.flexContainer}>
         {props.player&& (<div className={style.flexNode}><PlayerSelector 
              label="Select Player"
              players={players} 
              activePlayer={props.player} 
              onChange={(player)=>onPlayerSelect(player)}/></div>)}
               
    {props.player&& (
        <div className={style.flexNode}>
        <RadioButtonGroup className={style.flexContainer} name="shipSpeed" defaultSelected="vs">
      <RadioButton
        className={style.radio}
        value="vs"
        onClick={()=>onModeSelect('vs')}
        label="Versus"
      />
      <RadioButton
        className={style.radio}
        onClick={()=>onModeSelect('with')}
        value="with"
        label="With"
      />

    </RadioButtonGroup></div>)}
    </div>
        <Table onRowSelection={(row)=>{console.log('Row Selection',row)}}>
                        <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}>
                            <TableRow>
                                <TableHeaderColumn>Player</TableHeaderColumn>
                                <TableHeaderColumn>Played</TableHeaderColumn>
                                <TableHeaderColumn>Won</TableHeaderColumn>
                                <TableHeaderColumn>Draw</TableHeaderColumn>
                                <TableHeaderColumn>Lost</TableHeaderColumn>
                                <TableHeaderColumn>For</TableHeaderColumn>
                                <TableHeaderColumn>Against</TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                        <TableBody
                            displayRowCheckbox={false}>
                            {Object.keys(scores).map((key)=>{
                                const score:ScoreSheet = scores[key];
                                return (
                                <TableRow onMouseDown={()=>{
                                    onPlayerSelect(score.player)}} key={key}>
                                    <TableRowColumn>{key}</TableRowColumn>
                                    <TableRowColumn>{score.played}</TableRowColumn>
                                    <TableRowColumn>{score.win}</TableRowColumn>
                                    <TableRowColumn>{score.draw}</TableRowColumn>
                                    <TableRowColumn>{score.loss}</TableRowColumn>
                                    <TableRowColumn>{score.for}</TableRowColumn>
                                    <TableRowColumn>{score.against}</TableRowColumn>
                                </TableRow>
                            );})}
                        </TableBody>
                    </Table>
                    </div>);
}



export default ActionTable;

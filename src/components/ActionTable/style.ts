import {style} from 'typestyle';

export const root = style({
    backgroundColor:'#E1F5FE',
    height:'100vh',
    top:0,
    left:0,
    width:'100%'
});

export const paddingContainer = style({
    backgroundColor:'red',
    height:'3em',
    width: '100%'
});

export const createPlayer = style({
    position:'fixed',
    bottom: 45,
    right:50,
});

export const radio = style({
    paddingTop:'1em',
});
export const flexContainer = style({
    display:'flex',
    flexDirection:'row',
    justifyContent:'center',
});

export const flexNode = style({
    display:'flex',
    justifyContent:'center',
    flexGrow:1,
    
})



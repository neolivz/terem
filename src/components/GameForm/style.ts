import {style} from "typestyle";

export const scoreContainer = style({
    display:'flex',
    flexGrow:0,
    textAlign:'center',
})

export const scoreWrapper = style({
    height: 100,
    width: 100,
    margin: 20,
    textAlign: 'center',
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
})
export const score = style({
    textAlign: 'center',
    fontSize: '1em',
    border: 'none',
    background: 'transparent',
    paddingLeft: '0.6em',
})
export const vs = style({
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
})


export const root = style({
    $debugName:'root_game_',
    display:'flex',
    flexDirection:'column',
    backgroundColor:'#fff',
    margin:'2em',
    paddingLeft:'1em',
    paddingRight:'1em',
    paddingBottom:'2em',
    borderRadius:100,
})

export const centerContainer = style({
    flexGrow:0,
    flexDirection:'column',
    justifyContent:'center',
    display:'flex',
    paddingLeft:'1em',
    paddingRight:'1em',
    fontSize:'2em',

})
export const actionsContainer = style({
    display:'flex',
    flexDirection:'row-reverse',
})

export const gameConatiner = style({
    display:'flex',
    width:'auto',
    $debugName:'gameConatiner_',
})

export const playersContainer = style({
    display:'flex',
    flexDirection:'column',
    flexGrow:1,
})

export const player = style({
    display:'flex',
    flexGrow: 1,
})

export const playerSelector = style({
    backgroundColor: '#fff',
    $debugName:'playerSelector_',
})
import * as React from 'react';

import DatePicker from 'material-ui/DatePicker';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';


import { Game } from '../../core/reducers/gameReducer';
import { Player } from '../../core/reducers/playerReducer';
import { PlayerSelector } from '../PlayerSelector';
import { Score } from '../Score';

import * as style from './style';

interface GameFormProps extends Game {
  players: Player[];
  onChange: (game:Game) => any;
  onSave: (game: Game) => any;
}

const filterPlayers = (players:Player[], filterPlayerArray:(Player|undefined)[]):Player[] => {  
  return players.filter((player: Player)=>{
    return filterPlayerArray.find((filterPlayer: Player)=>{
            return filterPlayer && filterPlayer.name === player.name;
    }) == undefined;    
  });
};

export const GameForm = (props: GameFormProps) => {
    const {players, player1, player2, player3, player4, date, score1, score2, onSave, onChange} = props;
    const game:Game = { player1, player2, player3, player4, date, score1, score2};
    return (<Paper className={style.root} zDepth={4} >
        <div className = {style.gameConatiner}>
          <div className ={style.playersContainer}>
            <div className ={style.player}>
              <PlayerSelector 
              label="Player1"
              players={filterPlayers(players,[player2, player3, player4])} 
              activePlayer={player1} 
              onChange={(player1)=>onChange({...game, player1})}/>
            </div>
            <div className ={style.player}>
              <PlayerSelector 
              label="Player2 (Optional)"
              optional={true}
              players={filterPlayers(players,[player1, player3, player4])}               activePlayer={player2} 
              onChange={(player2)=> onChange({...game, player2})}/>
            </div>

          </div>
          <div className={style.centerContainer}>
            <div className ={style.scoreContainer}>
              <Score score={score1}
              onChange={(score1:number)=>onChange({...game,score1})}/>

              <div className={style.vs}>
                Vs
              </div>
              <Score score={score2}
              onChange={(score2:number)=>onChange({...game,score2})}/>

            </div>
            <DatePicker hintText="Date" container="inline" mode="landscape"
                          onChange={(event, date)=>{event.preventDefault();onChange({...game, date})}}
                          />


          </div>
  
          <div className ={style.playersContainer}>
            <div className ={style.player}>
              <PlayerSelector 
              label="Player3"
              players={filterPlayers(players,[player1, player2, player4])} 
              activePlayer={player3} 
              onChange={(player3)=>onChange({...game, player3})}/>
            </div>
            <div className ={style.player}>
              <PlayerSelector 
              label="Player4 (Optional)"
              optional={true}
              players={filterPlayers(players,[player1, player2, player3])} 
              activePlayer={player4} 
              onChange={(player4)=>onChange({...game, player4})}/>
            </div>
          </div> 
        </div>
        <div className={style.actionsContainer}>
            <RaisedButton label="Create" onClick={()=>onSave(game)}primary={true} style={style} />
        </div>
      </Paper>);
}



export default GameForm;

import * as React from 'react';

import AppBar from 'material-ui/AppBar';
import {Link} from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { lightBlueA700 } from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import * as injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();



const muiTheme = getMuiTheme({
  palette: {
    primary1Color: lightBlueA700,
  },
  appBar: {
    height: 50,
  },
});

class App extends React.Component<{}, {}> {
    render(): JSX.Element {
        const { children } = this.props;
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <Link to="/">
                        <AppBar title="Foosball Score"
                            onLeftIconButtonTouchTap={()=>{}}
                            iconElementLeft={<div></div>}/>
                    </Link>

                      {children}
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;

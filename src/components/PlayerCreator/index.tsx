import * as React from 'react';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import {Player} from '../../core/reducers/playerReducer';




import * as style from './style';

interface PlayerCreatorProps {
  onSave: (player:Player) => any;
  onClose: () => any;
  onChange: (player:Player) => any;
  player: Player;
  open: boolean;
}


export const PlayerCreator = (props: PlayerCreatorProps) => {
    const {player} = props;
    const actions = [
      <RaisedButton
        label="Save"
        primary={true}
        keyboardFocused={true}
        onTouchTap={ ()=>{props.onSave(player); }}
      />,
      <RaisedButton
        label="Cancel"
        secondary={true}
        keyboardFocused={true}
        onTouchTap={props.onClose}
      />
    ];
      return (
        <div className = {style.root}>
            <Dialog
                title="Create Player"
                actions={actions}
                modal={false}
                open={props.open}
                onRequestClose={props.onClose}
            >

            <TextField
              onChange={(event, name)=>{event.preventDefault();props.onChange({...player,name})}}
              hintText="Enter the player name"
              floatingLabelText="Player Name"
              value={player.name}
              floatingLabelFixed={false}
            />
          </Dialog>
        </div>
      )
}



export default PlayerCreator;

import * as React from 'react';

import GameForm from '../GameForm';
import PlayerCreator from '../PlayerCreator';

import { bindActionCreators } from 'redux';
import { saveGame, updateGame, GameActions } from '../../core/actions/gameActions';
import { openCreatePlayerDialog, savePlayer, updatePlayer } from '../../core/actions/playerAction';
import { RootState } from '../../core/reducers/';
import { Game } from '../../core/reducers/gameReducer';
import { Player } from '../../core/reducers/playerReducer';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import FontIcon from 'material-ui/FontIcon';

import * as style from './style';


const { connect } = require('react-redux');

type GameProperties = GameStateProperties & GameDispatchProperties;
interface GameStateProperties{
    game: Game;
    players: Player[];
    open: boolean;
    player: Player;

}
interface GameDispatchProperties{
    onSave: ()=>GameActions;//TODO: Fix it
    onChange: (game:Game)=>void;
    updatePlayer: (player:Player)=>void;
    savePlayer: (player:Player) => void;
    openCreatePlayerDialog: (open:boolean) => void;

}

const mapStateToProps = (state: RootState):GameStateProperties => {
    return {
        game: state.game,
        players: state.players,
        player: state.player,
        open: state.app.openCreatePlayerDialog,
    }
}
const mapDispatchToProps = (dispatch:any) => {
    return bindActionCreators({
        onSave:saveGame, 
        onChange:updateGame,
        updatePlayer,
        savePlayer,
        openCreatePlayerDialog
    }, dispatch);
};


@connect(mapStateToProps, mapDispatchToProps)
class GameCreator extends React.Component<GameProperties, {}> {
    render(): JSX.Element {
        const game = Object.assign(
            {score1:0,score2:0, date:new Date() },this.props.game);
        const {  player1, player2, player3, player4, date, score1, score2} = game;
        const { players, player, onChange, onSave, open,
            updatePlayer, savePlayer, openCreatePlayerDialog } = this.props;
        return <div>
            <GameForm 
                players={players} 
                player1={player1}
                player2={player2}
                player3={player3}
                player4={player4}
                score1={score1}
                score2={score2}
                date={date}
                onChange={onChange}
                onSave={onSave}/>
             <FloatingActionButton 
                onTouchTap={()=>openCreatePlayerDialog(true)}
                secondary={true} 
                className={style.createPlayer}>
                <FontIcon className="fa  fa-user-plus" /> 
            </FloatingActionButton>
            <PlayerCreator 
                open={open} 
                player={player}
                onChange={updatePlayer}
                onClose={()=>openCreatePlayerDialog(false)}
                onSave={savePlayer}/>
        </div>;
    }
}



export default GameCreator;

import * as React from 'react';

interface HelloProps {
    readonly to: string;
}

const Hello: React.StatelessComponent<HelloProps> = ({ to }) => <div>
    Hello {to}!
</div>;

class Welcome extends React.Component<{}, {}> {
    render(): JSX.Element {
        const {children} = this.props;
        return <div>
            <div>
                <Hello to="Welcome" />
            </div>
            {children}
            <div>
                This is the home page!
            </div>
        </div>;
    }
}

export default Welcome;

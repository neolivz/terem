import * as React from 'react';

import Paper from 'material-ui/Paper';



import * as style from './style';

interface GameCreatorProps {
  score:number;
  onChange: (value:any)=> void;
}


export const Score = (props: GameCreatorProps) => (
  <Paper className={style.scoreWrapper} zDepth={5} circle={true} >
    <input onChange={(evnt:any)=>props.onChange(parseInt(evnt.target.value))} className={style.score} value={props.score} type="number"/>
  </Paper>
             
)



export default Score;

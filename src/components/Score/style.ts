import {style} from "typestyle";


export const scoreWrapper = style({
    height: 100,
    width: 100,
    margin: 20,
    textAlign: 'center',
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
})
export const score = style({
    textAlign: 'center',
    fontSize: '1em',
    border: 'none',
    background: 'transparent',
    paddingLeft: '0.6em',
})
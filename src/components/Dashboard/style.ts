import {style} from 'typestyle';

export const root = style({
    backgroundColor:'#E1F5FE',
    height:'100vh',
    top:0,
    left:0,
    width:'100%'
});

export const paddingContainer = style({
    backgroundColor:'red',
    height:'3em',
    width: '100%'
});

export const createButton = style({
    position: 'fixed',
    bottom: 45,
    right:50
});
export const tableContainer = style({
    overflowY: 'scroll',
});

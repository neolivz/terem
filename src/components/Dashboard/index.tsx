import * as React from 'react';

import { Link } from 'react-router';
import { bindActionCreators } from 'redux';


import FloatingActionButton from 'material-ui/FloatingActionButton';
import FontIcon from 'material-ui/FontIcon';

import { selectDashboardPlayer, changeDashboardPlayerMode, VS_WITH } from '../../core/actions/playerAction';
import { loadScores } from '../../core/actions/scoresActions';
import { RootState } from '../../core/reducers/';
import { Player } from '../../core/reducers/playerReducer';
import { ScoreSheets } from '../../core/reducers/scoresReducer';

import { ActionTable } from '../ActionTable/';

import * as style from './style';

const { connect } = require('react-redux');
type DashProperties = DashStateProperties & DashDispatchProperties;
interface DashStateProperties{
    scores:ScoreSheets;
    player?:Player;
    filter?:VS_WITH;

}
interface DashDispatchProperties{
    loadScores:any;
    selectDashboardPlayer:any;
    changeDashboardPlayerMode:any;
}

const mapStateToProps = (state: RootState):DashStateProperties => {
    return {
        scores: state.scores,
        player: state.app.activePlayer,
        filter: state.app.filterMode,
    };
};
const mapDispatchToProps = (dispatch:any) => {
    return bindActionCreators({loadScores,selectDashboardPlayer, changeDashboardPlayerMode
    }, dispatch);
};
@connect(mapStateToProps, mapDispatchToProps)
class Dashboard extends React.Component<DashProperties, {}> {
    render(): JSX.Element {

        return (
            <div>
                <div className={style.tableContainer}>
                    
                    <ActionTable
                        scores={this.props.scores}
                        player={this.props.player}
                        filter={this.props.filter}
                        onModeSelect={(vswith)=>this.props.changeDashboardPlayerMode(vswith)}
                        onPlayerSelect={(player)=>{this.props.selectDashboardPlayer(player)}}
                    />
                </div>

                <Link to="/game-center">
                    <FloatingActionButton secondary={true} className={style.createButton}>
                        <FontIcon className="fa fa-plus" /> 
                    </FloatingActionButton>
                </Link>
            </div>
        );
    }
}

export default Dashboard;
;

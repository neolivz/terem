import * as React from 'react';

import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

import { Player } from '../../core/reducers/playerReducer';

interface PlayerSelectorProps {
  players: Player[];
  activePlayer: Player | undefined;
  label: string;
  optional?:boolean;
  onChange: (payload: Player) => void;
}


export const PlayerSelector = (props: PlayerSelectorProps) => {
    const {activePlayer} = props;
  return (
      <SelectField
          labelStyle={{backgroundColor:'#fff', color:'#000'}}
          style={{color:'#000'}}
          floatingLabelText={props.label}
          value={activePlayer?activePlayer.name :null}
          onChange={(event, key, val)=>{
              console.log(event,key);//TODO: Not sure how to remove the warning
              props.onChange({name:val});
            }}
          fullWidth={true}
        >
          {props.optional && <MenuItem value={null} primaryText="" />}
          {props.players.map(
            (player) => <MenuItem key={player.name} value={player.name} primaryText={player.name}/>
          )}
        </SelectField>
)}



export default PlayerSelector;

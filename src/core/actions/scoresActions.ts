import * as localforage from 'localforage';

import {
    ASYNC, 
    ASYNC_FAILED, 
    ASYNC_STARTED, 
    ASYNC_SUCCEEDED,
    GAMES,
    LOAD_SCORES,
    SAVE_GAME,
} from './actionTypes';

import { Dispatcher } from '../../core/reducers';
import { Game } from '../../core/reducers/gameReducer';
import { ScoreSheets } from '../../core/reducers/scoresReducer';
// import { Player } from '../../core/reducers/playerReducer';

export type LoadScores = {
    type: LOAD_SCORES
    status: ASYNC,
    payload: Game[],
    error?: Error
}
export type UpdateScores = {
    type: SAVE_GAME
    status: ASYNC,
    payload: Game[],
    error?: Error
}

type ScoreSheetDispatcher<A> = Dispatcher<A, ScoreSheets>;


export const loadScores = () => (
    async (dispatch: ScoreSheetDispatcher<LoadScores> ) =>{
        dispatch({
            type: LOAD_SCORES, 
            status: ASYNC_STARTED,
            payload: []
        });
        try {
            const games: Game[] = await localforage.getItem(GAMES) as Game[] || [];
            dispatch({
                type: LOAD_SCORES, 
                status: ASYNC_SUCCEEDED,
                payload: games
            });
        } 
        catch(error){
            dispatch({
                type: LOAD_SCORES, 
                status: ASYNC_FAILED,
                payload: [],
                error
            });
        }
});


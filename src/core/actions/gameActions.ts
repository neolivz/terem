import * as localforage from 'localforage';

import {
    ASYNC, 
    ASYNC_FAILED, 
    ASYNC_STARTED, 
    ASYNC_SUCCEEDED,
    GAME_UPDATE, 
    GAMES,
    LOAD_GAMES, 
    SAVE_GAME} from './actionTypes';

import { Dispatcher } from '../../core/reducers';
import { Game } from '../../core/reducers/gameReducer';


export type GameActions = SaveGame | GameUpdate | LoadGames;

export type SaveGame = {
    type: SAVE_GAME
    status: ASYNC,
    payload:Game,
    error?: Error
};

export type GameUpdate = {
    type: GAME_UPDATE
    status: ASYNC,
    payload: Game,
    error?: Error
}
export type LoadGames = {
    type: LOAD_GAMES
    status: ASYNC,
    payload: Game[],
    error?: Error
}


type GameDispatcher<A> = Dispatcher<A, Game>;






export const saveGame = (game: Game) => (
    async (dispatch: GameDispatcher<SaveGame> ) =>{
        dispatch({
            type: SAVE_GAME, 
            status: ASYNC_STARTED,
            payload: game
        });

        try {        
            const games: Game[] = await localforage.getItem(GAMES) as Game[] || [];
            if(!game.player1 || !game.player3 || !game.date || game.score1 === undefined || 
            game.score2 === undefined){
                throw new Error('Some Fileds are missing or invalid');
            }
            await localforage.setItem(GAMES, [...games, game]);
            dispatch({
                type: SAVE_GAME, 
                status: ASYNC_SUCCEEDED,
                payload: game
            });
        } 
        catch(error) {
            dispatch({
                type: SAVE_GAME, 
                status: ASYNC_FAILED,
                payload: game,
                error: error
            });
        }
});

export const updateGame = (game: Game) => ({
    type: GAME_UPDATE, 
    payload: game
});

export const loadGames = () => (
    async (dispatch: GameDispatcher<LoadGames> ) =>{
        dispatch({
            type: LOAD_GAMES, 
            status: ASYNC_STARTED,
            payload: []
        });
        try {
            const games: Game[] = await localforage.getItem(GAMES) as Game[] || [];

            dispatch({
                type: LOAD_GAMES, 
                status: ASYNC_SUCCEEDED,
                payload: games
            });
        } 
        catch(error){
            dispatch({
                type: LOAD_GAMES, 
                status: ASYNC_FAILED,
                payload: [],
                error: error
            });
        }
});


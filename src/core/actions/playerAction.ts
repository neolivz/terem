import * as localforage from 'localforage';

import {
    ASYNC, 
    ASYNC_FAILED, 
    ASYNC_STARTED, 
    ASYNC_SUCCEEDED,
    CHANGE_DASHBOARD_PLAYER_MODE,
    CREATE_PLAYER_DIALOG,
    LOAD_PLAYERS, 
    PLAYER_UPDATE,
    PLAYERS, 
    SAVE_PLAYER,
    SELECT_DASHBOARD_PLAYER,
} from './actionTypes';

import { Dispatcher } from '../../core/reducers';
import { Player } from '../../core/reducers/playerReducer';


export type PlayerActions = PlayerUpdate | SavePlayer ;
export type PlayersActions = LoadPlayers | SavePlayer ;
export type VS_WITH ='vs'|'with';

export type SelectDashboardPlayer = {
    type: SELECT_DASHBOARD_PLAYER
    payload:Player,
};
export type ChangeDashboardPlayerMode = {
    type: CHANGE_DASHBOARD_PLAYER_MODE
    payload:VS_WITH,
};
export type SavePlayer = {
    type: SAVE_PLAYER
    status: ASYNC,
    payload:Player,
    error?: Error
};

export type LoadPlayers = {
    type: LOAD_PLAYERS
    status: ASYNC,
    payload: Player[],
    error?: Error
};

export type PlayerUpdate = {
    type: PLAYER_UPDATE
    payload: Player
};

export type CreatePlayerDialog = {
    type: CREATE_PLAYER_DIALOG;
    payload: boolean;
};

export const openCreatePlayerDialog = (open:boolean):CreatePlayerDialog => ({
    type: CREATE_PLAYER_DIALOG, 
    payload: open,
});

export const selectDashboardPlayer = (player:Player):SelectDashboardPlayer => ({
    type: SELECT_DASHBOARD_PLAYER, 
    payload: player,
});

export const changeDashboardPlayerMode = (vswith:VS_WITH):ChangeDashboardPlayerMode => ({
    type: CHANGE_DASHBOARD_PLAYER_MODE, 
    payload: vswith,
});




type PlayerDispatcher<A> = Dispatcher<A, Player>;



export const savePlayer = (player: Player) => (
    async (dispatch: PlayerDispatcher<SavePlayer> ) =>{
        dispatch({
            type: SAVE_PLAYER, 
            status: ASYNC_STARTED,
            payload: player
        });

        try {        
            const players: Player[] = await localforage.getItem(PLAYERS) as Player[] || [];
            const el = players.find( (filterPlayer: Player) => {
                return (filterPlayer.name === player.name);
            });
            if(el !== undefined){
                //Player Already Exists
                throw new Error('Player Already Exists');

            }

            await localforage.setItem(PLAYERS, [...players, player]);
            dispatch({
                type: SAVE_PLAYER, 
                status: ASYNC_SUCCEEDED,
                payload: player
            });
        } 
        catch(error) {
            dispatch({
                type: SAVE_PLAYER, 
                status: ASYNC_FAILED,
                payload: player,
                error: error
            });
        }
});

export const updatePlayer = (player: Player):PlayerUpdate =>  ({
    type: PLAYER_UPDATE, 
    payload: player
});

export const loadPlayers = () => (
    async (dispatch: PlayerDispatcher<LoadPlayers> ) =>{
        dispatch({
            type: LOAD_PLAYERS, 
            status: ASYNC_STARTED, 
            payload: []
        });

        try {
            const players: Player[] = await localforage.getItem(PLAYERS) as Player[] || [];
            dispatch({
                type: LOAD_PLAYERS, 
                status: ASYNC_SUCCEEDED,
                payload: players
            });
        } 
        catch(error){
            dispatch({
                type: LOAD_PLAYERS, 
                status: ASYNC_FAILED,
                payload: [],
                error: error
            });
        }
});


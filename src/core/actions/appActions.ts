import * as types from './actionTypes';

export type AppAction = AppStartedAction |
    AppStartFailedAction |
    AppStartKickedAction;

export type AppStartKickedAction = {
    type: types.APP_START_KICKED
};

export type AppStartedAction = {
    type: types.APP_STARTED
};

export type AppStartFailedAction = {
    type: types.APP_START_FAILED
    reason: string
};





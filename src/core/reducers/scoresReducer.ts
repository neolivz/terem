import { Game } from './gameReducer';
import { ASYNC_FAILED, ASYNC_STARTED, ASYNC_SUCCEEDED, LOAD_SCORES, SAVE_GAME}  from '../actions/actionTypes';


import { LoadScores } from '../actions/scoresActions';
import { SaveGame } from '../actions/gameActions';

import {Player} from './playerReducer';

export interface ScoreSheets{
    [name:string]:ScoreSheet;
}

export interface ScoreSheet {
    player: Player;
    played: number;
    win: number;
    loss: number;
    draw: number;
    for: number;
    against: number;
    vs:ScoreSheets;
    with:ScoreSheets;
}

const reduce = (obj:ScoreSheets, el:Game) => {
    const addToPlayerVs = (
        player:Player, 
        player2:Player,
        score1:number, 
        score2:number) => {
            const vs = obj[player.name].vs = Object.assign({},obj[player.name].vs);
            addToPlayer(vs, player2, score1, score2);
    }
    const addToPlayerWith = (
        player:Player, 
        player2:Player,
        score1:number, 
        score2:number) => {
            const _with = obj[player.name].with = Object.assign({},obj[player.name].with);
            addToPlayer(_with, player2, score1, score2);
    }
    const addToPlayer =(
        obj:ScoreSheets,
        player:Player,
        score1:number,
        score2:number) =>{
            obj[player.name] = obj[player.name]? Object.assign({},obj[player.name]) : {
                player, played:0, win:0, loss:0, draw:0, for:0, against:0, vs:{}, with:{}
                    };
            obj[player.name].played++; 
            score1 === score2? obj[player.name].draw++: score1 > score2 ?
            obj[player.name].win++: obj[player.name].loss++;
            obj[player.name].for+=score1;
            obj[player.name].against += score2;
    };

    // createScoreSheet(player:Player, oppo: Player, date:Date, score1:number, score2: number){
    //     addToPlayer(player, score1, score2);
    //     addToPlayer(player2, score1, score2);
    // };
    const { player1, player2, player3, player4, score1, score2 } = el;


    if(!player1 || !player3) {
        throw Error('Data Error');
    }
    addToPlayer(obj, player1, score1, score2);
    addToPlayer(obj, player3, el.score2, el.score1);
    addToPlayerVs(player1, player3, score1, score2);
    addToPlayerVs(player3, player1, score2, score1);
    if(player2){
        addToPlayer(obj, player2, score1, score2);
        addToPlayerVs(player2, player3, score1, score2);
        addToPlayerVs(player3, player2, score2, score1);
        addToPlayerWith(player2, player1, score1, score2);
        addToPlayerWith(player1, player2, score1, score2);
    }
    if(player4){
        addToPlayer(obj, player4, score1, score2);
        addToPlayerVs(player4, player1, score2, score1);
        addToPlayerVs(player1, player4, score1, score2);
        addToPlayerWith(player4, player3, score2, score1);
        addToPlayerWith(player3, player4, score2, score1);
    }
    if(player2 && player4){
        addToPlayerVs(player4, player2, score2, score1);
    addToPlayerVs(player2, player4, score1, score2);
    }
    return obj;
    
}


export const scoreReducer = (state: ScoreSheets={}, action: LoadScores|SaveGame): ScoreSheets => {
    switch(action.type) {

        case LOAD_SCORES: {
            const games: Game[] = action.payload;

            if(action.status === ASYNC_STARTED ){
                return {} //We are still loading players
            } 
            else if(action.status === ASYNC_FAILED){
                return state; //Return current state of the store
            } 
            else if(action.status === ASYNC_SUCCEEDED){
                state = Object.assign({},state); //New object to keep it immutable
                games.map((game:Game)=>{
                    reduce(state,game);
                });
                return state; //We have the new players loaded
            }
            else {
                return state; //We have the new players loaded
            }

        }
        case SAVE_GAME:{
            const game:Game = action.payload;
            if(action.status === ASYNC_SUCCEEDED){
                state = Object.assign({},state);
                reduce(state,game);
            }
            return state;
        }
        default:
            return state;
    }
};

export default scoreReducer;

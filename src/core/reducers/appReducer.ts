import * as types from '../actions/actionTypes';
import { CreatePlayerDialog, SavePlayer, SelectDashboardPlayer, ChangeDashboardPlayerMode, VS_WITH } from '../actions/playerAction';
import { Player } from '../reducers/playerReducer'; 

export interface AppState {
    openCreatePlayerDialog: boolean;
    activePlayer?:Player;
    filterMode?: VS_WITH;
}


 


export const appReducer = (state = { openCreatePlayerDialog:false }, action: SavePlayer|CreatePlayerDialog|SelectDashboardPlayer|ChangeDashboardPlayerMode): AppState => {
    switch(action.type) {
        case types.CREATE_PLAYER_DIALOG:
            return { ...state, 
                openCreatePlayerDialog: action.payload
            };
        case types.SAVE_PLAYER:{
            if(action.status ===types.ASYNC_STARTED){
                return { ...state, 
                    openCreatePlayerDialog: false
                };
            } else if(action.status === types.ASYNC_FAILED) {
                return { ...state, 
                    openCreatePlayerDialog: true
                };
            } else {
                return state;
            }
        }
        case types.SELECT_DASHBOARD_PLAYER:
            return { ...state, 
                activePlayer: action.payload
            };
        case types.CHANGE_DASHBOARD_PLAYER_MODE:{
            return { ...state, 
                filterMode: action.payload
            };
        }
        
        default:
            return state;
    }
};


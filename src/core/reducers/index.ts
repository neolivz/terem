import { routerReducer } from 'react-router-redux';
import { combineReducers, Dispatch } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { appReducer, AppState } from './appReducer';
import { gameReducer, Game } from './gameReducer';
import { gamesReducer } from './gamesReducer';
import { playerReducer, Player } from './playerReducer';
import { playersReducer } from './playersReducer';
import { scoreReducer, ScoreSheets } from './scoresReducer';



export interface RootState {
    readonly app: AppState;
    readonly game: Game;
    readonly games: Game[];
    readonly player: Player;
    readonly players: Player[];
    readonly scores: ScoreSheets;
}

export type Dispatcher<A,R> = (arg: A)=>Dispatch<R>;

export const rootReducer = combineReducers<RootState>({
    app: appReducer,
    form: formReducer,
    routing: routerReducer,
    game: gameReducer,
    games: gamesReducer,
    scores: scoreReducer,
    player: playerReducer,
    players: playersReducer,

});


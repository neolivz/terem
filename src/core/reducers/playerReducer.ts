import { ASYNC_FAILED, ASYNC_STARTED, SAVE_PLAYER, PLAYER_UPDATE}  from '../actions/actionTypes';

// type SEX =  'M'|'F';

export interface Player{
  name: string;
}

import { PlayerActions } from '../actions/playerAction';



export const playerReducer = (state: Player = {name:''}, action: PlayerActions): Player => {
    switch(action.type) {
        case SAVE_PLAYER: {
            const player: Player = action.payload;

            if(action.status === ASYNC_STARTED ){
                return { name:'' }; //Successful remove the player from the store
            } 
            else if(action.status === ASYNC_FAILED){
                return player; //We change to edit to back the same player
            } 
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }

        }
        case PLAYER_UPDATE:{
            return action.payload;
        }
        default:
            return state;
    }
};

export default playerReducer;

import { ASYNC_FAILED, ASYNC_STARTED, SAVE_PLAYER, LOAD_PLAYERS}  from '../actions/actionTypes';

import { Player } from './playerReducer';


import { PlayersActions } from '../actions/playerAction';



export const playersReducer = (state: Player[] = [], action: PlayersActions): Player[] => {
    switch(action.type) {
        case SAVE_PLAYER: {
            const player: Player = action.payload;


            if(action.status === ASYNC_STARTED ){
                return [...state, player]
            } 
            else if(action.status === ASYNC_FAILED){
                const index = state.indexOf(player);
                return [
                    ...state.slice(0, index),
                    ...state.slice(index +1)]
            } 
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }

        }
        case LOAD_PLAYERS: {
            const players: Player[] = action.payload;

            if(action.status === ASYNC_STARTED ){
                return []; //We are still loading players
            } 
            else if(action.status === ASYNC_FAILED){
                return state; //Return current state of the store
            } 
            else {
                return players; //We have the new players loaded
            }

        }
        default:
            return state;
    }
};

export default playersReducer;

import { Game } from './gameReducer';
import { ASYNC_FAILED, ASYNC_STARTED, LOAD_GAMES, SAVE_GAME}  from '../actions/actionTypes';


import { GameActions } from '../actions/gameActions';



export const gamesReducer = (state: Game[] =[], action: GameActions): Game[] => {
    switch(action.type) {
        case SAVE_GAME: {
            const game: Game = action.payload;


            if(action.status === ASYNC_STARTED ){
                return [...state, game];//We show the game when async starts
            } 
            else if(action.status === ASYNC_FAILED){
                const index = state.indexOf(game);
                return [
                    ...state.slice(0, index),
                    ...state.slice(index +1)];//And removed it if it fails
            } 
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }

        }
        case LOAD_GAMES: {
            const games: Game[] = action.payload;

            if(action.status === ASYNC_STARTED ){
                return []; //We are still loading players
            } 
            else if(action.status === ASYNC_FAILED){
                return state; //Return current state of the store
            } 
            else {
                return games; //We have the new players loaded
            }

        }
        default:
            return state;
    }
};

export default gamesReducer;

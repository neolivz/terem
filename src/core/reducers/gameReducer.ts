import { Player } from './playerReducer';
import { ASYNC_FAILED, ASYNC_STARTED, GAME_UPDATE, SAVE_GAME}  from '../actions/actionTypes';


export interface Game{
    date?: Date;
    player1?: Player;
    player2?: Player;
    player3?: Player;
    player4?: Player;
    score1: number;
    score2: number;
}


import { GameActions } from '../actions/gameActions';



export const gameReducer = (state: Game | {} = {}, action: GameActions): Game|{} => {
    switch(action.type) {
        case SAVE_GAME: {
            const game: Game = action.payload;

            if(action.status === ASYNC_STARTED ){
                return {}; //Successful remove the player from the store
            } 
            else if(action.status === ASYNC_FAILED){
                return game; //We change to edit to back the same player
            } 
            else {
                //ASYNC_SUCCEEDED does not need to change anything as we shown as saved the moment it started
                return state;
            }

        }
        case GAME_UPDATE: {
            return action.payload;
        }
        default:
            return state;
    }
};

export default gameReducer;

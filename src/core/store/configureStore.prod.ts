import { applyMiddleware, compose, createStore, StoreEnhancerStoreCreator } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer, RootState} from '../reducers';

const middlewares: (next: StoreEnhancerStoreCreator<RootState>) => StoreEnhancerStoreCreator<RootState> = 
    applyMiddleware(thunk);

const configureStore = (initialState: RootState) => createStore<RootState>(
    rootReducer,
    initialState,
    compose(middlewares)
);

export default configureStore;

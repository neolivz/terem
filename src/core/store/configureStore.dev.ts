import { applyMiddleware, compose, createStore, StoreEnhancerStoreCreator } from 'redux';
import * as reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import * as createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import { rootReducer, RootState} from '../reducers';

const logger = createLogger();
const devExtension = (window as any).devToolsExtension;
const middlewares: (next: StoreEnhancerStoreCreator<RootState>) => StoreEnhancerStoreCreator<RootState> = 
    applyMiddleware(thunk, reduxImmutableStateInvariant(), logger);

const configureStore = (initialState: RootState) => createStore<RootState>(
    rootReducer,
    initialState,
    compose(
        middlewares,
        devExtension ? devExtension() : f => f
    )
);

export default configureStore;
